'use strict';

/**
 * ************************************************
 * Gulp Packages
 * ************************************************
 */
import gulp from 'gulp';
import jshint from 'gulp-jshint';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import sass from 'gulp-ruby-sass';
import postCss from 'gulp-postcss';
import postCssAutoprefixer from 'autoprefixer';
import postCssMqPacker from 'css-mqpacker';
import postCssNano from 'cssnano';

/**
 * ************************************************
 * Autoprefixer browsers
 * ************************************************
 */
const autoprefixer_browsers = [
    'ie >= 8',
    'ie_mob >= 8',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 2.1',
    'bb >= 10'
];

/**
 * ************************************************
 * PostCSS Processors
 * @type {[*]}
 * ************************************************
 */
const postCssProcessors = [
    postCssNano( {
        discardComments: { removeAll: true }
    } ),
    postCssMqPacker( { sort : true } ),
    postCssAutoprefixer( { browsers : autoprefixer_browsers, grid: true } )
];


/**
 * Build Working Directory constants.
 * @type {{src: string, dist: string}}
 */
const dirs = {
    src: 'src/',
    dist: 'assets/',
};


/**
 * ************************************************
 * Global Style
 * @type {{watch: string, src: string, dist: string}}
 * ************************************************
 */
const globalStyle = {
    watch: `${dirs.src}sass/**/*.scss`,
    src: `${dirs.src}sass/style.scss`,
    dist: `${dirs.dist}css/`
};

/**
 * Global Style Task.
 */
gulp.task( 'global-scss', function () {
    return sass( globalStyle.src, {
        loadPath: ['node_modules/susy/sass']
    } )
        .on( 'error', sass.logError )
        .pipe( postCss( postCssProcessors ) )
        .pipe( gulp.dest( globalStyle.dist ) )
});

/**
 * Global Style Watch.
 */
gulp.task('watch-global-scss', function () {
    gulp.watch( globalStyle.watch, ['global-scss'] );
});

/**
 * ************************************************
 * Editor Style
 * @type {{watch: string, src: string, dist: string}}
 * ************************************************
 */
const editorStyle = {
    watch: `${dirs.src}sass/**/*.scss`,
    src: `${dirs.src}sass/editor-style.scss`,
    dist: `/`
};

/**
 * Editor Style Task.
 */
gulp.task( 'editor-scss', function () {
    return sass( editorStyle.src )
        .on( 'error', sass.logError )
        .pipe( postCss( postCssProcessors ) )
        .pipe( gulp.dest( editorStyle.dist ) )
});

/**
 * Editor Style Watch.
 */
gulp.task('watch-editor-scss', function () {
    gulp.watch( editorStyle.watch, ['editor-scss'] );
});


/**
 * ************************************************
 * Global Scripts
 * @type {{watch: string, distDir: string, distFile: string}}
 * ************************************************
 */
const globalScripts = {
        watch: `${dirs.src}js/**/*.js`,
        distDir: `${dirs.dist}js/`,
        distFile: 'scripts.js',
    };

/**
 * Global Script Task
 */
gulp.task('global-scripts', function () {
    return gulp.src( globalScripts.watch )
        .pipe( jshint() )
        .pipe( jshint.reporter('jshint-stylish') )
        .pipe( concat( globalScripts.distFile ) )
        .pipe( uglify() )
        .pipe( gulp.dest( globalScripts.distDir ) )
});

/**
 * Global Script Watch
 */
gulp.task('watch-global-scripts', function () {
    // Compile all scripts in the js lib directory into the processed js file
    gulp.watch( globalScripts.watch, ['global-scripts'] );
});

/**
 * ************************************************
 * Watch All
 * ************************************************
 */
gulp.task('watch-global-all', function () {
    // Style.
    gulp.watch( globalStyle.watch, ['global-scss'] );
    gulp.watch( editorStyle.watch, ['editor-scss'] );
    // Scripts.
    gulp.watch( globalScripts.watch, ['global-scripts'] );
});

var realFavicon = require ('gulp-real-favicon');
var fs = require('fs');

// File where the favicon markups are stored
var FAVICON_DATA_FILE = 'faviconData.json';

// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
gulp.task('generate-favicon', function(done) {
    realFavicon.generateFavicon({
        masterPicture: 'favicon.png',
        dest: 'assets/img/icons/',
        iconsPath: '/',
        design: {
            ios: {
                pictureAspect: 'backgroundAndMargin',
                backgroundColor: '#ffffff',
                margin: '14%',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: false,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                }
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'whiteSilhouette',
                backgroundColor: '#2b5797',
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: false,
                    windows10Ie11EdgeTiles: {
                        small: false,
                        medium: true,
                        big: false,
                        rectangle: false
                    }
                }
            },
            androidChrome: {
                pictureAspect: 'shadow',
                themeColor: '#ffffff',
                manifest: {
                    name: 'Montessori Institute New England',
                    display: 'standalone',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            },
            safariPinnedTab: {
                pictureAspect: 'silhouette',
                themeColor: '#2e3d66'
            }
        },
        settings: {
            compression: 5,
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false,
            readmeFile: false,
            htmlCodeFile: false,
            usePathAsIs: false
        },
    markupFile: FAVICON_DATA_FILE
}, function() {
        done();
    });
});

// Inject the favicon markups in your HTML pages. You should run
// this task whenever you modify a page. You can keep this task
// as is or refactor your existing HTML pipeline.
gulp.task('inject-favicon-markups', function() {
    return gulp.src([ 'assets/img/icons/favicons.html' ])
        .pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
        .pipe(gulp.dest('assets/img/icons/'));
});

// Check for updates on RealFaviconGenerator (think: Apple has just
// released a new Touch icon along with the latest version of iOS).
// Run this task from time to time. Ideally, make it part of your
// continuous integration system.
gulp.task('check-for-favicon-update', function(done) {
    var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
    realFavicon.checkForUpdates(currentVersion, function(err) {
        if (err) {
            throw err;
        }
    });
});
