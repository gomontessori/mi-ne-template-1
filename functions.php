<?php
/**
 * Site functions
 *
 * Functions specific to this child theme.
 *
 * @package WordPress
 * @subpackage GM Template 5
 * @since GM 1.0
 */

?>
<?php
/** Reload style sheets and scripts to load in correct order */
function gm_child_styles() {
	// Register child CSS.
	wp_register_style( 'gm-child', get_stylesheet_directory_uri() . '/assets/css/style.css', array( 'style', 'normalize' ), '1.0' );

	// Enqueue child CSS.
	wp_enqueue_style( 'gm-child' );

	wp_enqueue_script( 'webfontloader', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js','', 1.6, true );

	//wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/scripts.all.min.js', array( 'jquery' ), 1.1, true );

	wp_localize_script( 'script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu' ),
		'collapse' => __( 'collapse child menu' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'gm_child_styles' );

/** Add child theme editor styles */
function gm_child_add_editor_styles() {
	add_editor_style( 'editor-style.css' );
}
add_action( 'after_setup_theme', 'gm_child_add_editor_styles' );

/** Add Fonts to Editor Styles */
function gm_child_font_editor_styles() {
	$font_url = esc_url( 'Raleway:400,400i,700,700i&display=swap' );
	add_editor_style( $font_url );
}

add_action( 'after_setup_theme', 'gm_child_font_editor_styles' );

/** Remove parent themes buttons */
function gm_remove_parent_mce_formats() {
	remove_filter( 'tiny_mce_before_init', 'gm_mce_before_init_insert_formats' );
}
add_action( 'after_setup_theme', 'gm_remove_parent_mce_formats' );
/**Add Child Custom Buttons to Editor */
function gm_child_mce_before_init_insert_formats( $init_array ) {

	// Define the style_formats array.
	$style_formats = array(
		// Each array child is a format with it's own settings.
		array(
			'title' => 'Blue Button',
			'inline' => 'a',
			'classes' => 'btn btn-blue',
			'selector' => 'a',
			'wrapper' => true,

		),
		array(
			'title' => 'Green Button',
			'inline' => 'a',
			'classes' => 'btn btn-green',
			'selector' => 'a',
			'wrapper' => true,
		),
		array(
			'title' => 'Sky Blue Button',
			'inline' => 'a',
			'classes' => 'btn btn-sky',
			'selector' => 'a',
			'wrapper' => true,

		),
		array(
			'title' => 'Blue',
			'inline' => 'span',
			'classes' => 'blue',
			'wrapper' => true,
		),
		array(
			'title' => 'Yellow',
			'inline' => 'span',
			'classes' => 'yellow',
			'wrapper' => true,
		),
		array(
			'title' => 'Sky Blue',
			'inline' => 'span',
			'classes' => 'sky',
			'wrapper' => true,
		)
	);
	// Insert the array, JSON ENCODED, into 'style_formats'.
	$init_array['style_formats'] = wp_json_encode( $style_formats );

	return $init_array;
}
// Attach callback to 'tiny_mce_before_init'.
add_filter( 'tiny_mce_before_init', 'gm_child_mce_before_init_insert_formats' );

/** Remove parent nav to customize child nav. Uncomment if needed */
/* add_action( 'after_setup_theme', 'gm_remove_parent_nav' );

	function gm_remove_parent_nav() {
		unregister_nav_menu( 'primary-menu' );
		unregister_nav_menu( 'social-links' );
	}

	function gm_child_nav_main()
	{
		wp_nav_menu(
		array(
			'theme_location'  => 'primary',
			'menu_class'      => 'primary-menu',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'container'       => false,
			)
		);
	}

	function gm_child_nav_social() {
		wp_nav_menu(
		array(
			'theme_location' => 'social',
			'menu_class'     => 'social-links-menu',
			'depth'          => 1,
			'link_before'    => '<span class="screen-reader-text">',
			'link_after'     => '</span>',
			'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
			'container'       => false,
			)
		);
	}
	// Register Child theme Navigation.
	function gm_register_childtheme_menu()
	{
		register_nav_menus(array( // Using array to specify more menus if needed
			'primary' => __( 'Header Menu' ), // Main Navigation
			'social' => __( 'Footer Sitemap' ), // Footer Navigation
		));
	}
	add_action( 'init', 'gm_register_childtheme_menu' ); // Add Child Blank Menu
	*/


/**Hide ACF field group menu item */

//add_filter('acf/settings/show_admin', '__return_false');

/** Save theme-specific ACF field groups */
add_filter('acf/settings/save_json', function() {

	$paths = get_template_directory() . '/acf-json/';

	if ( is_child_theme() ) {
		$paths = get_stylesheet_directory() . '/acf-json/';
	}

	return $paths;

});

add_filter( 'acf/settings/load_json', function( $paths ) {

	unset( $paths[0] );

	$paths = array();

	if ( is_child_theme () )
	{
		$paths[] = get_stylesheet_directory() . '/acf-json';
	}
	$paths[] = get_template_directory() . '/acf-json';

	return $paths;
});
