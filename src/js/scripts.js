(function( root, $, undefined ) {
	"use strict";

	$(function () {

        // Remove style attribute on image captions, which sets a fixed width by default
        $('.wp-caption').removeAttr('style');

        //Flexslider
        // Can also be used with $(document).ready()
        $(document).ready(function() {
            $('.flexslider').flexslider({
                animation: "fade",
                controlNav: false,
                prevText: '',
                nextText: '',
                smoothHeight: true,

                start: function(slider) {
                    slider.removeClass('loading');
                }
            });
        });

        //Make WordPress iframe oembeds responsive
        var $all_oembed_videos = $("iframe");

        $all_oembed_videos.each(function() {

            $(this).removeAttr('height').removeAttr('width').wrap( "<div class='embed-container'></div>" );

        });

        //Expanding boxes for FAQ section
        $('.accordion-content').hide();
        //$('h2.section-title:first').addClass('active').next().show();

        $('h3.accordion-title').on('click', function(){

            if( $(this).next().is(':hidden') ) {

                $('h3.accordion-title').removeClass('active').next().slideUp();
                $(this).toggleClass('active').next().slideDown();

            } else {
                $('h3.accordion-title').removeClass('active').next().slideUp();
            }
            return false;
        });

        // Search overlay
        $('.js-search-btn').on('click', function(){
            $('.js-search-overlay').addClass('is-open');
        })

        $('.js-search')
            .on('click', function(){
                $('.js-search-overlay').removeClass('is-open');
            })
            .on('click', '.js-search-wrap', function(e){
                e.stopPropagation();
            })
            .bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(e){
                $('.js-search-input').focus();
            });

        //DROPDOWN RESPONSIVE MENU FROM TWENTYSIXTEEN THEME

        var body, masthead, menuToggle, siteNavigation, socialNavigation, siteHeaderMenu, resizeTimer;

        function initMainNavigation( container ) {

            // Add dropdown toggle that displays child menu items.
            var dropdownToggle = $( '<button />', {
                'class': 'dropdown-toggle',
                'aria-expanded': false
            } ).append( $( '<span />', {
                'class': 'screen-reader-text',
                text: screenReaderText.expand
            } ) );

            container.find( '.menu-item-has-children > a' ).after( dropdownToggle );

            // Toggle buttons and submenu items with active children menu items.
            container.find( '.current-menu-ancestor > button' ).addClass( 'toggled-on' );
            container.find( '.current-menu-ancestor > .sub-menu' ).addClass( 'toggled-on' );

            // Add menu items with submenus to aria-haspopup="true".
            container.find( '.menu-item-has-children' ).attr( 'aria-haspopup', 'true' );

            container.find( '.dropdown-toggle' ).click( function( e ) {
                var _this            = $( this ),
                    screenReaderSpan = _this.find( '.screen-reader-text' );

                e.preventDefault();
                _this.toggleClass( 'toggled-on' );
                _this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );

                // jscs:disable
                _this.attr( 'aria-expanded', _this.attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
                // jscs:enable
                screenReaderSpan.text( screenReaderSpan.text() === screenReaderText.expand ? screenReaderText.collapse : screenReaderText.expand );
            } );

        }
        initMainNavigation( $( '.main-navigation' ) );

        masthead         = $( '#masthead' );
        menuToggle       = masthead.find( '#menu-toggle' );
        siteHeaderMenu   = masthead.find( '#site-header-menu' );
        siteNavigation   = masthead.find( '#site-navigation' );
        socialNavigation = masthead.find( '#social-navigation' );

        // Enable menuToggle.
        ( function() {

            // Return early if menuToggle is missing.
            if ( ! menuToggle.length ) {
                return;
            }

            // Add an initial values for the attribute.
            menuToggle.add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded', 'false' );

            menuToggle.on( 'click.twentysixteen', function() {
                $( this ).add( siteHeaderMenu ).toggleClass( 'toggled-on' );

                // jscs:disable
                $( this ).add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded', $( this ).add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
                // jscs:enable
            } );
        } )();

        // Fix sub-menus for touch devices and better focus for hidden submenu items for accessibility.
        ( function() {
            if ( ! siteNavigation.length || ! siteNavigation.children().length ) {
                return;
            }

            // Toggle `focus` class to allow submenu access on tablets.
            function toggleFocusClassTouchScreen() {
                if ( window.innerWidth >= 910 ) {
                    $( document.body ).on( 'touchstart.twentysixteen', function( e ) {
                        if ( ! $( e.target ).closest( '.main-navigation li' ).length ) {
                            $( '.main-navigation li' ).removeClass( 'focus' );
                        }
                    } );
                    siteNavigation.find( '.menu-item-has-children > a' ).on( 'touchstart.twentysixteen', function( e ) {
                        var el = $( this ).parent( 'li' );

                        if ( ! el.hasClass( 'focus' ) ) {
                            e.preventDefault();
                            el.toggleClass( 'focus' );
                            el.siblings( '.focus' ).removeClass( 'focus' );
                        }
                    } );
                } else {
                    siteNavigation.find( '.menu-item-has-children > a' ).unbind( 'touchstart.twentysixteen' );
                }
            }

            if ( 'ontouchstart' in window ) {
                $( window ).on( 'resize.twentysixteen', toggleFocusClassTouchScreen );
                toggleFocusClassTouchScreen();
            }

            siteNavigation.find( 'a' ).on( 'focus.twentysixteen blur.twentysixteen', function() {
                $( this ).parents( '.menu-item' ).toggleClass( 'focus' );
            } );
        } )();

        // Add the default ARIA attributes for the menu toggle and the navigations.
        function onResizeARIA() {
            if ( window.innerWidth < 910 ) {
                if ( menuToggle.hasClass( 'toggled-on' ) ) {
                    menuToggle.attr( 'aria-expanded', 'true' );
                } else {
                    menuToggle.attr( 'aria-expanded', 'false' );
                }

                if ( siteHeaderMenu.hasClass( 'toggled-on' ) ) {
                    siteNavigation.attr( 'aria-expanded', 'true' );
                    socialNavigation.attr( 'aria-expanded', 'true' );
                } else {
                    siteNavigation.attr( 'aria-expanded', 'false' );
                    socialNavigation.attr( 'aria-expanded', 'false' );
                }

                menuToggle.attr( 'aria-controls', 'site-navigation social-navigation' );
            } else {
                menuToggle.removeAttr( 'aria-expanded' );
                siteNavigation.removeAttr( 'aria-expanded' );
                socialNavigation.removeAttr( 'aria-expanded' );
                menuToggle.removeAttr( 'aria-controls' );
            }
        }

        //Improve skip to content button
        ( function() {
            var isWebkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
                isOpera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
                isIE     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

            if ( ( isWebkit || isOpera || isIE ) && document.getElementById && window.addEventListener ) {
                window.addEventListener( 'hashchange', function() {
                    var id = location.hash.substring( 1 ),
                        element;

                    if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
                        return;
                    }

                    element = document.getElementById( id );

                    if ( element ) {
                        if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
                            element.tabIndex = -1;
                        }

                        element.focus();

                        // Repositions the window on jump-to-anchor to account for admin bar and border height.
                        window.scrollBy( 0, -53 );
                    }
                }, false );
            }
        } )();
	});


} ( this, jQuery ));

// LIGHT VIDEO EMBED FOR HERO AREAS

document.addEventListener('DOMContentLoaded', function() {
    lightEmbedInit();
});

function lightEmbedInit() {

    var div, n,
        v = document.getElementsByClassName('video-player');

    for (n = 0; n < v.length; n++) {

        v[n].onclick = function() {

            var id = this.dataset.id;
            var img_src = new URL(this.children[0].attributes[0].value);
            var hostname = img_src.hostname;

            var iframe = document.createElement('iframe');

            if (hostname == 'i.ytimg.com') {
                var embed = 'https://www.youtube.com/embed/ID?autoplay=1';
            }
            else if (hostname == 'i.vimeocdn.com') {
                var embed = '//player.vimeo.com/video/ID?autoplay=1';
            }

            iframe.setAttribute('src', embed.replace('ID', id));
            iframe.setAttribute('frameborder', '0');
            iframe.setAttribute('allowfullscreen', '1');
            this.parentNode.replaceChild(iframe, this);
        };
    }
}
