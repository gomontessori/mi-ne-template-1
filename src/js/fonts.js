(function( root, $, undefined ) {
	"use strict";

	$(function () {

      WebFont.load({
        google: {
            families: [ 'Raleway:400,400i,700,700i,800,900' ]
        }
      });

	});

} ( this, jQuery ));
